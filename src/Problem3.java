import java.util.Scanner;

public class Problem3 {
    public static void main(String[] args) {
        int firstNum, secondNum;
        Scanner sc = new Scanner(System.in);
        System.out.println("Please input first number");
        firstNum = sc.nextInt();
        int x = firstNum;
        System.out.println("Please input second number");
        secondNum = sc.nextInt();
        int y = secondNum;
        if (x > y) {
            System.out.println("Error");
        } else {
            for (int i = x; i <= y; i++)
                System.out.print(i + " ");
        }
    }

}
